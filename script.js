const tabs = Array.from(document.getElementsByClassName('tabs-title'));
const tabsDescr = Array.from(document.getElementsByClassName('tab-descr'));

document.querySelector('.tabs').addEventListener('click', function (event) {
    if(event.target.classList.contains('tabs-title')) {
        tabs.forEach(item => item.classList.remove('active'));
        event.target.classList.add('active');
        tabsDescr.forEach(item => {
            item.classList.remove('active');
            if(event.target.dataset.tab === item.dataset.tab)
                item.classList.add('active')
        })
    }
});
